/**
 * 任务管理器
 */
public class JobManagement {

    private static volatile JobManagement management;

    private JobManagement() {

    }

    public static synchronized JobManagement getManagement() {

        if (null == management) {
            System.out.println("未创建任务管理器，正在创建。。");
            management = new JobManagement();
        } else {
            System.out.println("已经存在创建的任务管理器");
        }
        return management;
    }
}
