package impl;

import interfaces.PhoneFactory;
import pojo.PhoneProduct;

public class XiaoMiPhoneFactory implements PhoneFactory {
    @Override
    public PhoneProduct show() {
        return new PhoneProduct(1, "小米10 Pro");
    }
}
