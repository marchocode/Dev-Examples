package impl;

import interfaces.ElectricalFactory;
import pojo.ElectricalProduct;

public class XiaoMiElectricalFactory implements ElectricalFactory {
    @Override
    public ElectricalProduct show() {
        return new ElectricalProduct(1, "小米扫地机器人");
    }
}
