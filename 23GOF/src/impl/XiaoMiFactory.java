package impl;

import interfaces.AbstractFactory;
import interfaces.ElectricalFactory;
import interfaces.PhoneFactory;

public class XiaoMiFactory implements AbstractFactory {

    @Override
    public PhoneFactory phoneFactory() {
        return new XiaoMiPhoneFactory();
    }
    @Override
    public ElectricalFactory electricalFactory() {
        return new XiaoMiElectricalFactory();
    }
}
