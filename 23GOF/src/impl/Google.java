package impl;

import abstracts.SearchSubject;

public class Google extends SearchSubject {
    @Override
    public void search() {
        System.out.println("Google 搜索引擎");
    }
}
