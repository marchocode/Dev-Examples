package proxys;

import abstracts.SearchSubject;
import impl.Google;

/**
 * VPN 代理
 * 静态代理也需要实现抽象主题
 */
public class VPNProxy extends SearchSubject {
    /**
     * 含有真实主题
     */
    private Google google;

    @Override
    public void search() {
        if (null == google) {
            google = new Google();
        }
        this.before();
        /**
         * 调用真实对象的方法
         */
        google.search();
        this.after();
    }
    /**
     * 增强方法
     */
    public void before() {
        System.out.println("VPN 开始执行。。。");
    }
    public void after() {
        System.out.println("VPN 结束执行");
    }
}
