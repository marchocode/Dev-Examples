package pojo;

public class PhoneProduct {

    private int id;

    private String name;

    public PhoneProduct(int id, String name) {
        this.id = id;
        this.name = name;
    }
    @Override
    public String toString() {
        return "手机铭牌 编号：" + id + ",型号：" + name;
    }
}
