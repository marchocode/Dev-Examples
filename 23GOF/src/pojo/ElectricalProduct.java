package pojo;

/**
 * 电器产品
 */
public class ElectricalProduct {

    private int id;

    private String name;

    public ElectricalProduct(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return "电器铭牌：编号：" + id + ",名称：" + name;
    }
}
