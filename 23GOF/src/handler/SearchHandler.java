package handler;

import interfaces.SearchInterface;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * 搜索处理器
 */
public class SearchHandler implements InvocationHandler {

    private void before() {
        System.out.println("handler start");
    }

    private void after() {
        System.out.println("handler stop");
    }

    private SearchInterface obj;

    public SearchHandler(SearchInterface obj) {
        this.obj = obj;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        this.before();
        /**
         * 执行代理方法
         */
        Object result = method.invoke(obj, args);
        System.out.println("result=" + result.toString());
        this.after();

        return result;
    }
}
