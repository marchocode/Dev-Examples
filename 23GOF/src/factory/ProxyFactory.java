package factory;

import interfaces.SearchInterface;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

/**
 * 动态代理工厂
 */
public class ProxyFactory {
    /**
     * 目标对象
     */
    private SearchInterface object;
    private InvocationHandler handler;

    public ProxyFactory(SearchInterface obj, InvocationHandler handler) {
        this.object = obj;
        this.handler = handler;
    }
    /**
     * 获取代理对象
     * @return
     */
    public Object getProxyObj() {
        ClassLoader classLoader = object.getClass().getClassLoader();
        Class<?>[] interfaces = object.getClass().getInterfaces();
        return Proxy.newProxyInstance(classLoader, interfaces, handler);
    }
}
