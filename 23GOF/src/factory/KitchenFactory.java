package factory;

import impl.HongShao;
import impl.TangCu;
import interfaces.FoodProduct;

public class KitchenFactory {
    /**
     * id=1 上菜红烧肉 id=2 糖醋鱼
     * @param id
     */
    public FoodProduct cooking(int id) {
        if (1 == id) {
            return new HongShao();
        } else {
            return new TangCu();
        }
    }
}
