package interfaces;

/**
 * 搜索接口
 */
public interface SearchInterface {
    String search();
}
