package interfaces;

import pojo.PhoneProduct;

public interface PhoneFactory {
    /**
     * 手机工厂可以做的事情
     */
    PhoneProduct show();
}
