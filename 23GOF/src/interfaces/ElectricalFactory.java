package interfaces;

import pojo.ElectricalProduct;

public interface ElectricalFactory {
    /**
     * 电器工厂可以生产电器
     */
    ElectricalProduct show();
}
