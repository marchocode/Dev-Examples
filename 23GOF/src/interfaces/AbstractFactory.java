package interfaces;

public interface AbstractFactory {
    /**
     * 创建手机工厂
     * @return
     */
    PhoneFactory phoneFactory();
    /**
     * 创建电器工厂
     * @return
     */
    ElectricalFactory electricalFactory();
}
