package abstracts;

/**
 * 抽象主题，可以进行搜索
 */
public abstract class SearchSubject {
    /**
     * 可以进行搜索的操作
     */
    public abstract void search();
}
