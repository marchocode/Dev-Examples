import client.AbstractFactoryBuilder;
import factory.KitchenFactory;
import factory.ProxyFactory;
import handler.SearchHandler;
import impl.GoogleSearch;
import impl.XiaoMiFactory;
import interfaces.AbstractFactory;
import interfaces.FoodProduct;
import interfaces.PhoneFactory;
import interfaces.SearchInterface;
import pojo.PhoneProduct;
import proxys.VPNProxy;

import java.io.*;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.Arrays;

public class Client {

    public static void main(String[] args) throws Exception {

        /**
         * obj1
         */
//        JobManagement management1 = JobManagement.getManagement();
//        System.out.println(management1);
//        JobManagement management2 = JobManagement.getManagement();
//        System.out.println(management2);

        /**
         * obj2
         */
//        Teacher teacher = new Teacher(1, "张老师");
//
//        Student student1 = new Student(1, "李四", teacher);
//        Student student2 = student1.clone();
//
//        System.out.println(student1);
//        System.out.println(student2);
//
//        System.out.println(student1 == student2);

        /**
         * obj3
         */
//        Teacher teacher = new Teacher(1, "张老师");
//        System.out.println(teacher);
//
//        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
//        ObjectOutputStream stream = new ObjectOutputStream(outputStream);
//
//        stream.writeObject(teacher);
//        System.out.println(Arrays.toString(outputStream.toByteArray()));
//
//        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(outputStream.toByteArray());
//        ObjectInputStream inputStream = new ObjectInputStream(byteArrayInputStream);
//
//        Teacher teacher1 = (Teacher) inputStream.readObject();
//        System.out.println(teacher1);

        /**
         * obj4
         */

//        KitchenFactory kitchen = new KitchenFactory();
//        FoodProduct food = kitchen.cooking(1);
//        food.show();


//        AbstractFactory factory = new XiaoMiFactory();
//
//        PhoneFactory phoneFactory = factory.phoneFactory();
//        phoneFactory.show();


        /**
         * obj5
         */
//        AbstractFactoryBuilder factoryClient = new AbstractFactoryBuilder();
//        PhoneProduct product = factoryClient.createPhone("xiaomi");
//        System.out.println(product);


        /**
         * obj6 静态代理
         */
        //VPNProxy proxy = new VPNProxy();
        //proxy.search();

        /**
         * obj7 动态代理
         */
        //SearchInterface search = new GoogleSearch();

        //System.out.println("1id=" + search);
        //InvocationHandler handler = new SearchHandler(search);

        //ProxyFactory factory = new ProxyFactory(search, handler);
        //SearchInterface google = (SearchInterface) factory.getProxyObj();

        //System.out.println("2id=" + google);
        //google.search();

        /**
         * obj8 Cglib 子类代理
         */






    }
}
