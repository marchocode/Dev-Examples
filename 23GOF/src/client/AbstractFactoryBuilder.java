package client;

import impl.HuaweiFactory;
import impl.XiaoMiFactory;
import interfaces.AbstractFactory;
import interfaces.PhoneFactory;
import pojo.PhoneProduct;

public class AbstractFactoryBuilder {

    public PhoneProduct createPhone(String name) {

        AbstractFactory factory = null;

        if ("xiaomi" == name) {
            factory = new XiaoMiFactory();
        } else {
            factory = new HuaweiFactory();
        }
        PhoneFactory phoneFactory = factory.phoneFactory();

        return phoneFactory.show();
    }
}
