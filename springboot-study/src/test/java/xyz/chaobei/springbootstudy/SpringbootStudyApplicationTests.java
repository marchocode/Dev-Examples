package xyz.chaobei.springbootstudy;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import xyz.chaobei.springbootstudy.entity.OrgRoleEntity;
import xyz.chaobei.springbootstudy.service.OrgRoleService;

@SpringBootTest
@Slf4j
class SpringbootStudyApplicationTests {

    @Autowired
    private OrgRoleService orgRoleService;

    @Test
    void contextLoads() {

        UpdateWrapper wrapper = new UpdateWrapper<>();
        wrapper.eq("id_",7);

        wrapper.set("lock_",2);

        boolean isSave =  orgRoleService.update(wrapper);
        log.info(isSave + "");
    }

}
