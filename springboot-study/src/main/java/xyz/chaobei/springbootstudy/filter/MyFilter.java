package xyz.chaobei.springbootstudy.filter;

import lombok.extern.java.Log;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Log
@WebFilter(filterName = "myFilter", urlPatterns = {"/api/*"})
public class MyFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        log.info("myFilter init");
    }
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;

        log.info("myFilter doFilter");
        log.info("myFilter url=" + request.getRequestURI());
        log.info("myFilter type=" + request.getMethod());
        log.info("myFilter sessionId=" + request.getSession().getId());
        log.info("myFilter IP local=" + request.getLocalAddr());
        log.info("myFilter IP remote=" + request.getRemoteAddr());

        filterChain.doFilter(servletRequest,servletResponse);
    }

    @Override
    public void destroy() {
        log.info("myFilter destroy");
    }
}
