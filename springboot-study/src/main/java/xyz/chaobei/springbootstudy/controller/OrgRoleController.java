package xyz.chaobei.springbootstudy.controller;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.chaobei.springbootstudy.service.OrgRoleService;

@RestController
@RequestMapping
public class OrgRoleController {

    @Autowired
    private OrgRoleService orgRoleService;

    public void getOne () {

        UpdateWrapper wrapper = new UpdateWrapper<>();
        wrapper.eq("id",7);

        wrapper.set("lock_",2);

        orgRoleService.update(wrapper);
    }

}
