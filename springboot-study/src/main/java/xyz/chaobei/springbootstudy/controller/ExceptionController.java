package xyz.chaobei.springbootstudy.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.chaobei.springbootstudy.config.ResultModel;
import xyz.chaobei.springbootstudy.exception.PageNotFoundException;
import xyz.chaobei.springbootstudy.exception.RequestBadException;

@RestController
@RequestMapping("/api/exception")
public class ExceptionController {

    @RequestMapping("null")
    public ResultModel exe1() {
        throw new NullPointerException();
    }

    @RequestMapping("not")
    public ResultModel exe2() { throw new PageNotFoundException(); }

    @RequestMapping("runtime")
    public ResultModel exe3() {
        throw new RequestBadException();
    }

}
