package xyz.chaobei.springbootstudy.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import xyz.chaobei.springbootstudy.util.MediaTypeUtils;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Controller
@Slf4j
public class DownLoadController {

    private String DIRECTORY = "C:";

    private String fileName = "other.pdf";

    @Autowired
    private ServletContext servletContext;

    @RequestMapping("/down1")
    public ResponseEntity<InputStreamResource> downFile1() throws FileNotFoundException {

        MediaType mediaType = MediaTypeUtils.getMediaTypeForFileName(servletContext, fileName);
        log.info(mediaType.getType());

        File file = new File(DIRECTORY + "/" + fileName);
        InputStreamResource resource = new InputStreamResource(new FileInputStream(file));

        return ResponseEntity.ok()
                // Content-Disposition
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + file.getName())
                // Content-Type
                .contentType(mediaType)
                // Contet-Length
                .contentLength(file.length()) //
                .body(resource);
    }

    @RequestMapping("/down2")
    public ResponseEntity<ByteArrayResource> down2() throws IOException {

        MediaType mediaType = MediaTypeUtils.getMediaTypeForFileName(servletContext, fileName);
        log.info(mediaType.getType());

        Path path = Paths.get(DIRECTORY + "/" + fileName);
        byte[] data = Files.readAllBytes(path);
        ByteArrayResource resource = new ByteArrayResource(data);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + fileName)
                .contentType(mediaType)
                .contentLength(data.length).body(resource);
    }


    @RequestMapping("/down3")
    public void down3(HttpServletResponse response) throws IOException {

        MediaType mediaType = MediaTypeUtils.getMediaTypeForFileName(servletContext, fileName);
        log.info(mediaType.getType());

        Path path = Paths.get(DIRECTORY + "/" + fileName);
        byte[] data = Files.readAllBytes(path);

        InputStream inputStream = Files.newInputStream(path);

        response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + fileName);
        response.setContentType(mediaType.getType());
        response.setContentLength(data.length);

        BufferedOutputStream outputStream = new BufferedOutputStream(response.getOutputStream());

        byte[] bytes = new byte[1024];
        int index = 0;
        while ((index = inputStream.read(bytes)) != -1) {
            outputStream.write(bytes, 0, index);
        }

        outputStream.flush();
        inputStream.close();
    }
}
