package xyz.chaobei.springbootstudy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.chaobei.springbootstudy.config.AppConfig;
import xyz.chaobei.springbootstudy.config.SimpleProperties;

@RestController
@RequestMapping("/api/config")
public class ConfigController {

    @Value("${study.version}")
    private String version;

    @Autowired
    private SimpleProperties simpleProperties;

    @Autowired
    private AppConfig appConfig;

    @RequestMapping("version")
    public ResponseEntity version() {
        return ResponseEntity.ok(version);
    }

    @RequestMapping("simple")
    public ResponseEntity simple() {
        return ResponseEntity.ok(simpleProperties);
    }

    @RequestMapping("app")
    public ResponseEntity app() {
        return ResponseEntity.ok(appConfig);
    }

}
