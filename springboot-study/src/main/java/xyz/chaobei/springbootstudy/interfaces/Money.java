package xyz.chaobei.springbootstudy.interfaces;

import xyz.chaobei.springbootstudy.validator.MoneyValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 金额验证器 接受元金额
 * 比如 10 10.1 10.10 都可以 但是10. 不能接受
 */
@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = MoneyValidator.class)
public @interface Money {

    String message() default "金额格式有误";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
