package xyz.chaobei.springbootstudy.entity;

import lombok.Data;
import xyz.chaobei.springbootstudy.interfaces.Enums;
import xyz.chaobei.springbootstudy.interfaces.Money;

import javax.validation.constraints.*;

@Data
public class Book {
    /**
     * 编号
     */
    @NotNull(message = "请填写编号")
    private Integer id;

    /**
     * 名称
     */
    @NotBlank(message = "请填写名称")
    private String name;

    /**
     * 价格HttpMessageNotReadableException
     */
    @NotBlank
    @Pattern(regexp = "^[0-9]+(.[0-9]{2})?$", message = "金额格式有误")
    private String price;

    @Money
    private Double prices;

    /**
     * 验证是否在枚举内
     */
    @Enums(value = {1,2,3},message = "请输入正确的类型")
    private Integer status;
}
