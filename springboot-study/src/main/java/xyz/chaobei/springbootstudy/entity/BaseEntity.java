package xyz.chaobei.springbootstudy.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.util.Date;

/**
 * 基础实体类、一般需要包含如下字段
 */
@Data
public class BaseEntity {

    @TableField(value = "create_time_", fill = FieldFill.INSERT)
    protected Date createTime;

    @TableField(value = "update_time_", fill = FieldFill.UPDATE)
    protected Date updateTime;

    @TableField(value = "status_", fill = FieldFill.INSERT)
    protected Integer status;
}
