package xyz.chaobei.springbootstudy.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 实体
 *
 * @author mrc
 */
@Data
@TableName("org_role")
public class OrgRoleEntity extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 角色表
     */
    @TableId(type = IdType.AUTO,value = "id_")
    private Integer id;


    /**
     * 角色名称
     */
    @TableField("name_")
    private String name;


    /**
     * 描述
     */
    @TableField("desc_")
    private String desc;


    /**
     * 角色锁 1正常 0锁定
     */
    @TableField("lock_")
    private Integer lock;
}
