package xyz.chaobei.springbootstudy.validator;

import xyz.chaobei.springbootstudy.interfaces.Enums;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.LinkedList;
import java.util.List;

public class EnumValidator implements ConstraintValidator<Enums, Integer> {

    private Enums enums;
    private List<Integer> value = new LinkedList<>();

    @Override
    public void initialize(Enums constraintAnnotation) {
        this.enums = constraintAnnotation;
        for (int num : enums.value()) {
            value.add(num);
        }
    }
    @Override
    public boolean isValid(Integer integer, ConstraintValidatorContext constraintValidatorContext) {
        return value.contains(integer);
    }
}
