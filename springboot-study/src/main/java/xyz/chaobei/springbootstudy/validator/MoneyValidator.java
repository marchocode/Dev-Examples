package xyz.chaobei.springbootstudy.validator;

import lombok.extern.java.Log;
import xyz.chaobei.springbootstudy.interfaces.Money;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Log
public class MoneyValidator implements ConstraintValidator<Money, Double> {

    @Override
    public boolean isValid(Double aDouble, ConstraintValidatorContext constraintValidatorContext) {
        log.info("金额验证器,参数=" + aDouble);
        if (null == aDouble) {
            return false;
        }
        return aDouble.toString().matches("^[0-9]+(.[0-9]{1,2})?$");
    }
}
