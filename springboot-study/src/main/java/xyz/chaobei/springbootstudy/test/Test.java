package xyz.chaobei.springbootstudy.test;

public class Test {

    public static void main(String[] args) {

        ProxyFactory proxyFactory = new ProxyFactory(new BingSearch());

        BingSearch bing = (BingSearch) proxyFactory.getProxy();
        bing.search();
    }

}
