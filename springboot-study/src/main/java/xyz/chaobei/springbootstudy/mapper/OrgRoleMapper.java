package xyz.chaobei.springbootstudy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import xyz.chaobei.springbootstudy.entity.OrgRoleEntity;

public interface OrgRoleMapper extends BaseMapper<OrgRoleEntity> {
}
