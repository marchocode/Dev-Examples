package xyz.chaobei.springbootstudy.enums;

import java.lang.annotation.Annotation;

/**
 * 书本状态枚举
 */
public enum BookStatusEnum{

    WAIT(1,"等待上架"),
    NORMAL(2,"正常"),
    OFF(3,"已下架");

    private Integer status;
    private String val;

    BookStatusEnum(Integer status, String val) {
        this.status = status;
        this.val = val;
    }
    public Integer getStatus() {
        return status;
    }
    public String getVal() {
        return val;
    }
}
