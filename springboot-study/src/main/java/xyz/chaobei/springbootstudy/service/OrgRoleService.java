package xyz.chaobei.springbootstudy.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import xyz.chaobei.springbootstudy.entity.OrgRoleEntity;
import xyz.chaobei.springbootstudy.mapper.OrgRoleMapper;

@Service
public class OrgRoleService extends ServiceImpl<OrgRoleMapper, OrgRoleEntity> {


}
