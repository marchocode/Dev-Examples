package xyz.chaobei.springbootstudy.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 以simple 开头的都将注入到这个对象里面
 */
@ConfigurationProperties(prefix = "simple")
@Data
public class SimpleProperties {

    private Integer id;

    private String name;
}
