package xyz.chaobei.springbootstudy.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import xyz.chaobei.springbootstudy.interceptor.MyInterceptor;
/**
 * @Author MRC
 * @Description 配置拦截器
 * @Date 14:15 2020/7/10
 **/
@Configuration
public class WebConfig implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new MyInterceptor()).addPathPatterns("/api/book/");
    }
}