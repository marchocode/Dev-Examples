package xyz.chaobei.springbootstudy.config;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResultModel {

    private static volatile ResultModel model = new ResultModel();

    private Integer code;

    private String message;

    private Object data;

    private ResultModel(){}

    public static ResultModel success(String message, Object data) {
        model.code = 1;
        model.data = data;
        model.message = message;
        return model;
    }

    public static ResultModel error(String message) {
        model.code = 0;
        model.data = null;
        model.message = message;
        return model;
    }


}
