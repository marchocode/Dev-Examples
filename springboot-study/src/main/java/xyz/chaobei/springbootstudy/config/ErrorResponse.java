package xyz.chaobei.springbootstudy.config;

import lombok.Getter;
import xyz.chaobei.springbootstudy.exception.BaseException;

import java.time.Instant;

@Getter
public class ErrorResponse {

    private int code;

    private int status;

    private String message;

    private String path;

    private Instant timestamp;

    private Object data;

    public ErrorResponse(BaseException ex, String path) {
        this(ex.getErrorCode().getCode(), ex.getErrorCode().getStatus().value(), ex.getErrorCode().getMessage(), path, null);
    }

    public ErrorResponse(BaseException ex, String path, Object data) {
        this(ex.getErrorCode().getCode(), ex.getErrorCode().getStatus().value(), ex.getErrorCode().getMessage(), path, data);
    }

    public ErrorResponse(int code, int status, String message, String path, Object data) {
        this.code = code;
        this.status = status;
        this.message = message;
        this.path = path;
        this.data = data;
        this.timestamp = Instant.now();
    }


}
