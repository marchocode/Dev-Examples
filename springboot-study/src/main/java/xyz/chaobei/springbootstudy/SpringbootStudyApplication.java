package xyz.chaobei.springbootstudy;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.ServletComponentScan;
import xyz.chaobei.springbootstudy.config.SimpleProperties;

@SpringBootApplication
@EnableConfigurationProperties({SimpleProperties.class})
@ServletComponentScan
@MapperScan("xyz.chaobei.springbootstudy.mapper")
public class SpringbootStudyApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootStudyApplication.class, args);
	}

}
