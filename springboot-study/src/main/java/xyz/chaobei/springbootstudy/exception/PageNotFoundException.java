package xyz.chaobei.springbootstudy.exception;

public class PageNotFoundException extends BaseException {

    public PageNotFoundException() {
        super(ErrorCode.REQUEST_NOT_FOUND);
    }

}
