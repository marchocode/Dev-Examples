package xyz.chaobei.springbootstudy.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public enum ErrorCode {

    REQUEST_VALIDATION_FAILED(101, HttpStatus.BAD_REQUEST, "请求数据格式有误"),
    REQUEST_NOT_FOUND(104, HttpStatus.NOT_FOUND, "请求数据未找到"),
    REQUEST_MESSAGE_NOT_READ(104, HttpStatus.BAD_REQUEST,"请求内容无法被识别");

    private final int code;

    private final HttpStatus status;

    private final String message;

    ErrorCode(int code, HttpStatus status, String message) {
        this.code = code;
        this.status = status;
        this.message = message;
    }
}
