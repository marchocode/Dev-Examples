package xyz.chaobei.springbootstudy.exception;

import lombok.Getter;

@Getter
public abstract class BaseException extends RuntimeException {

    private ErrorCode errorCode;

    public BaseException(ErrorCode errorCode) {
        super(errorCode.getMessage());
        this.errorCode = errorCode;
    }
}
