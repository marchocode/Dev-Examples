package xyz.chaobei.springbootstudy.exception;

public class RequestBadException extends BaseException {

    public RequestBadException() {
        super(ErrorCode.REQUEST_VALIDATION_FAILED);
    }
}
