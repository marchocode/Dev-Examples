package xyz.chaobei.springbootstudy.exception;

/**
 * JSON 消息无法正确被读取错误
 */
public class MessageNotReadException extends BaseException {

    public MessageNotReadException() {
        super(ErrorCode.REQUEST_MESSAGE_NOT_READ);
    }
}
